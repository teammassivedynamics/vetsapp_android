package com.bishop.vetsapp40;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link cambiaDatos_cliente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link cambiaDatos_cliente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class cambiaDatos_cliente extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public cambiaDatos_cliente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment cambiaDatos_cliente.
     */
    // TODO: Rename and change types and number of parameters
    public static cambiaDatos_cliente newInstance(String param1, String param2) {
        cambiaDatos_cliente fragment = new cambiaDatos_cliente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_cambia_datos_cliente, container, false);




        Button btnSave = (Button)v.findViewById(R.id.btnSaveChanges);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vi) {
                if(checkValues(v)) {
                    Toast.makeText(cambiaDatos_cliente.this.getActivity(), "Guardado", Toast.LENGTH_LONG).show();
                    try {
                        Thread.sleep(2000);
                        FragmentManager frMan = getFragmentManager();
                        frMan.beginTransaction().replace(R.id.Contenido_princ, new muestra_consultas()).commit();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else
                    Toast.makeText(cambiaDatos_cliente.this.getActivity(),"No se guardó",Toast.LENGTH_LONG).show();
            }

        });

        return v;
    }





    private void close(View v){
        View view = cambiaDatos_cliente.this.getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean checkValues(View v){
        TextView tVNombre = (TextView)v.findViewById(R.id.ednombre);
        TextView tVApels = (TextView)v.findViewById(R.id.editTextApels);
        TextView tVTelef = (TextView)v.findViewById(R.id.editTextTel);
        TextView tVTelef2 = (TextView)v.findViewById(R.id.editTextTel2);
        TextView tVdir = (TextView)v.findViewById(R.id.editTextDir);
        TextView tVmail = (TextView)v.findViewById(R.id.editTextCorreo);

        if(tVNombre.getText().toString().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo nombre vacío",Toast.LENGTH_LONG).show();
            tVNombre.requestFocus();
            return  false;
        }
        else if(tVApels.getText().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo Apellidos vacío",Toast.LENGTH_LONG).show();
            tVApels.requestFocus();
            return  false;
        }
        else if(tVTelef.getText().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo teléfono vacío",Toast.LENGTH_LONG).show();
            tVTelef.requestFocus();
            return  false;
        }
        else if(tVTelef2.getText().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo teléfono 2 vacío",Toast.LENGTH_LONG).show();
            tVTelef2.requestFocus();
            return  false;
        }
        else if(tVdir.getText().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo dirección vacío",Toast.LENGTH_LONG).show();
            tVdir.requestFocus();
            return  false;
        }
        else if(tVmail.getText().length()==0){
            Toast.makeText(cambiaDatos_cliente.this.getActivity(),"Campo e mail vacío",Toast.LENGTH_LONG).show();
            tVmail.requestFocus();
            return  false;
        }
        return true;
    }

    private boolean fillAll(String name, View v){
        //Método búsqueda con el nombre dado
        TextView tVNombre = (TextView)v.findViewById(R.id.ednombre);
        tVNombre.setText(name);
        TextView tVApels = (TextView)v.findViewById(R.id.editTextApels);
        tVApels.setText(name);
        TextView tVTelef = (TextView)v.findViewById(R.id.editTextTel);
        tVTelef.setText("4921593054");
        TextView tVTelef2 = (TextView)v.findViewById(R.id.editTextTel2);
        tVTelef2.setText("4921593055");
        TextView tVdir = (TextView)v.findViewById(R.id.editTextDir);
        tVdir.setText("C. Juan Escutia 19, el paraíso, Gpe");
        TextView tVmail = (TextView)v.findViewById(R.id.editTextCorreo);
        tVmail.setText("javier.bodom@gamail.com");

        return true;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public void onAttach(Context context) {

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
