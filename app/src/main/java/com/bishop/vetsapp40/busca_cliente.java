package com.bishop.vetsapp40;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bishop.vetsapp40.Adaptadores.Items_cliente;
import com.bishop.vetsapp40.Adaptadores.ListViewAdapter_clientes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link busca_cliente.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link busca_cliente#newInstance} factory method to
 * create an instance of this fragment.
 */
public class busca_cliente extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Bundle bndl = new Bundle();
    private ListView listaclientes = null;
    private ArrayList<Items_cliente> arrayTems_clientes = null;
    private ListViewAdapter_clientes adapter=null;
    public Context context;
    private ArrayList<Items_cliente> arrarListItem;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String Name="";
    View v;
    View vista;

    //IP de mi Url
    String IP ="http://vetsapp20180130022001.azurewebsites.net/Customer";

    //Ruta del web service
    String GET_CLIENTES= IP +"/getMedicalConsultationList";

    ObtenerWebService hiloConexion;

    private OnFragmentInteractionListener mListener;

    public busca_cliente() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment busca_cliente.
     */
    // TODO: Rename and change types and number of parameters
    public static busca_cliente newInstance(String param1, String param2) {
        busca_cliente fragment = new busca_cliente();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private static final String[] NOMBRES = new String[]{
            "Francisco Javier Castro Cortés", "Victor javier Castro Cortes","nidia javier","Perdro javier","Juan javier","Esmeralda javier","Francisca Javier","Gerardo Javier","Jose luis javir","Diana javier", "Victor2 javier", "Victor3 javier"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         v =  inflater.inflate(R.layout.fragment_busca_cliente, container, false);

        //busca_cliente.context = v.getContext();
       // ListViewAdapter_clientes LA = new ListViewAdapter_clientes();
        setHasOptionsMenu(true);
        ArrayAdapter<String> adapterNomb = new ArrayAdapter<String>(this.getActivity(), R.layout.items_busquedas, NOMBRES);

         EditText AtexV = (EditText)v.findViewById(R.id.AtVBuscCl);

/*
        Button btnBuscCL = (Button)v.findViewById(R.id.btnBuscCL);
        btnBuscCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vi) {
                //Toast.makeText(busca_cliente.this.getActivity().getApplicationContext(),"Es: "+Name,Toast.LENGTH_LONG).show();
                if(Name.length()>0)
                    verDetalleCl(Name, v);
                else {
                    Snackbar.make(v, "Es necesario buscar un nombre", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });*/

        listaclientes = (ListView)v.findViewById(R.id.ListVItemsClientes);
        arrayTems_clientes = new ArrayList<>();
        cargarlista();

        listaclientes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView idD = (TextView) v.findViewById(R.id.id);
                TextView nombrePer_ = (TextView) v.findViewById(R.id.nombrePer_);
                return false;
            }
        });
        final FragmentManager frMan = getActivity().getSupportFragmentManager();

        listaclientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                frMan.beginTransaction().replace(R.id.Contenido_princ, new detalles_gr_cliente()).commit();
            }
        });

        registerForContextMenu(listaclientes);
        return v;
    }




    private void cargarlista(){
        //String nombre, int id, String Direccion, String Telefono, String Telefono2, String CorreoE
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro cortes", 11, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Victor manuel castro cortes", 22,"Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Gerardo reyes robes", 321,"Calle eupidio 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro4", 442, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro5", 545, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro6", 6521, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro7", 71, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro8", 8, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro9", 9, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro10", 10, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro11", 11, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro12", 12, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro13", 13, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));
        arrayTems_clientes.add(new Items_cliente("Francisco javier castro14", 14, "Calle juan escutia 19", "786317238", "12312323","javier.bodom@gmail.com"));

        adapter = new ListViewAdapter_clientes(arrayTems_clientes,this.getActivity());
        listaclientes.setAdapter(adapter);
    }

    private void cargalista2(){
        hiloConexion =new ObtenerWebService();
        hiloConexion.execute(GET_CLIENTES,"1");
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private boolean verDetalleCl(String nom, View v){
        //metodo para mandar llamar fragment de detalle del cliente

        FragmentManager frM =  this.getActivity().getSupportFragmentManager();
        //detalle_cliente tC = new detalle_cliente();
        //bndl.putString("nombre_cl",nom);
        //tC.setArguments(bndl);
       // Fragment fragment = new Fragment();
        //fragment.setArguments(bndl);

        Preferences.savePreferenceString(busca_cliente.this.getActivity(),nom,Preferences.NOMBRE_BUSQ_USUARIO);
        frM.beginTransaction().replace(R.id.Contenido_princ, new detalle_cliente()).commit();
        //FragmentTransaction transaction =MainActivity.getSupportFragmentManager().beginTransaction();
        return true;
    }

    public void getAlert(String msg, String title){
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setIcon(v.getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));

        alert.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "Borrado",Toast.LENGTH_LONG).show();
            }

        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "No",Toast.LENGTH_LONG).show();
            }
        });
        alert.show();
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    //INflamos el layout del context menu, el menù agregar paciente en el bar comienza


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_add_client, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager  frMan = getActivity().getSupportFragmentManager();
        switch (item.getItemId()){
            case R.id.addPaciente:
                frMan.beginTransaction().replace(R.id.Contenido_princ, new Fragment_client()).commit();
                return true;
            default:
                    return super.onOptionsItemSelected(item);
        }


    }

    //INflamos el layout del context menu, el menù flotante comienza aquì
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(""+adapter.arrarListItem.get(info.position).getNombre());
        inflater.inflate(R.menu.menu_flot_cliente, menu);

    }
    //manejamos eventos click en el context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrar:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrarListItem.get(info.position).getId(),Toast.LENGTH_SHORT).show();
                getAlert("¿Desea borrar a "+adapter.arrarListItem.get(info.position).getNombre()+"?","Alerta Borrar");
                return true;
            case R.id.modificar:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrarListItem.get(info.position).getId(),Toast.LENGTH_SHORT).show();
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    public static Context busca_cliente1(){
        return null;
    }



    public class ObtenerWebService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }
                        String result1 = result.toString();
                        result1 = result1.replace('\\', '"');
                        //result1 = result1.replace('/', ' ' );

                        JSONObject respuestaJSON = new JSONObject(result1);
                        String resultJSON = respuestaJSON.getString("Status");

                        if(resultJSON.equals("1")){
                            //datosConsulta = resultJSON.getJSONObject(0).getJSONArray("consultas");
                            JSONArray consultaJSON = respuestaJSON.getJSONArray("JsonResults");

                            // JSONArray JsonsResults = respuestaJSON.getJSONArray("JsonResults");
                            //JSONArray consultaJSON = JsonsResults.getJSONArray(0);

                            for (int i=0;i<consultaJSON.length();i++){

                                arrayTems_clientes.add(new Items_cliente(
                                        consultaJSON.getJSONObject(i).getString("PeludoName"),
                                        consultaJSON.getJSONObject(i).getInt("ConsultationDateAndHour"),
                                        consultaJSON.getJSONObject(i).getString("Description"),
                                        consultaJSON.getJSONObject(i).getString("ConsultationPlace"),
                                        consultaJSON.getJSONObject(i).getString("RegistrationDate"),
                                        consultaJSON.getJSONObject(i).getString("AditionalInformation"))
                                );
                                adapter = new ListViewAdapter_clientes(arrayTems_clientes, getActivity());
                                listaclientes.setAdapter(adapter);
                            }
                        }
                        devuelve = respuestaJSON.getString("Message");;


                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return devuelve;
        }
    }
}
