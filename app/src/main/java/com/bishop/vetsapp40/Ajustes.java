package com.bishop.vetsapp40;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Ajustes extends AppCompatActivity {
private static SeekBar seekBar;
private TextView tiempo;
private int progress_val;
private Switch aSwitch;
private Button btnSave;
private TextView mensajeCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);
        btnSave = (Button) findViewById(R.id.btnSave);
        mensajeCliente = (TextView)findViewById(R.id.mensajeCliente);

        aSwitch = (Switch)findViewById(R.id.switchCump);
        if(Preferences.obtenerPreferenceBoolean(getApplicationContext(),Preferences.MENSAJES_CUMPLEANIOS))
            aSwitch.setChecked(true);
        else
            aSwitch.setChecked(false);

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Preferences.savePreferenceBoolean(getApplicationContext(),true,Preferences.MENSAJES_CUMPLEANIOS);
                    aSwitch.setChecked(true);
                }
                else{
                    Preferences.savePreferenceBoolean(getApplicationContext(),false,Preferences.MENSAJES_CUMPLEANIOS);
                    aSwitch.setChecked(false);
                }
            }
        });

        if(Preferences.ObtenerPreferenceString(getApplicationContext(),Preferences.MENSAJE_CLIENTE).equals(" "))
            Preferences.savePreferenceString(getApplicationContext(),"Buena tarde, para confirmar la cita para su mascota para hoy a las 3pm ", Preferences.MENSAJE_CLIENTE);

        mensajeCliente.setText(Preferences.ObtenerPreferenceString(getApplicationContext(),Preferences.MENSAJE_CLIENTE));
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Preferences.savePreferenceString(getApplicationContext(),mensajeCliente.getText().toString(), Preferences.MENSAJE_CLIENTE);
                Toast.makeText(getApplicationContext(),"Guardado",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Ajustes.this, MainActivity.class);
                    startActivity(i);
                    finish();
            }
        });

        tiempo = (TextView)findViewById(R.id.tvTiempo);
        seekBar =(SeekBar)findViewById(R.id.seekBar);
        seekBar.setMax(120);
        if(Preferences.ObtenerPreferenceString(getApplicationContext(),Preferences.MINUTOS_NOTIFICACION).equals(" "))
            Preferences.savePreferenceString(getApplicationContext(),"60", Preferences.MINUTOS_NOTIFICACION);

        seekBar.setProgress(Integer.parseInt( Preferences.ObtenerPreferenceString(getApplicationContext(),Preferences.MINUTOS_NOTIFICACION)));
        tiempo.setText(seekBar.getProgress()+" minutos");

        seekbar();
    }

    public void seekbar(){
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tiempo.setText(progress+" minutos");
                progress_val = progress;
                Preferences.savePreferenceString(getApplicationContext(),""+progress, Preferences.MINUTOS_NOTIFICACION);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    public boolean band =false;

    public boolean getAlert(){

        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle("Guardar datos");
        alert.setMessage("¿Desea guardar los datos ahora?");
        alert.setIcon(getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));
        alert.setButton(AlertDialog.BUTTON_POSITIVE, "Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),"Guardado",Toast.LENGTH_LONG).show();

            }
        });
        alert.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                band = false;
            }
        });
        alert.show();
        return false;
    }
}
