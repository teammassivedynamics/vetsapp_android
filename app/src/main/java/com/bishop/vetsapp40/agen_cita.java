package com.bishop.vetsapp40;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link agen_cita.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link agen_cita#newInstance} factory method to
 * create an instance of this fragment.
 */
public class agen_cita extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Button btnFecha, btnHora, btnCreaConsulta;
    private EditText etFecha, etHora, numCelular, nombrePeludo, RazonPrincipal, LugarConsulta, NotCl, NotProp ;
    private int hora, minuto, dia, mes, anio;
    private boolean notifCliente, notifiPropia;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public agen_cita() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment agen_cita.
     */
    // TODO: Rename and change types and number of parameters
    public static agen_cita newInstance(String param1, String param2) {
        agen_cita fragment = new agen_cita();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_agen_cita, container, false);

        btnFecha = (Button) v.findViewById(R.id.btnFecha);
        btnHora = (Button)v.findViewById(R.id.btnHora);

        etFecha = (EditText)v.findViewById(R.id.etFecha);
        etHora = (EditText)v.findViewById(R.id.etHora);
        numCelular = (EditText)v.findViewById(R.id.nunCelular);
        nombrePeludo = (EditText)v.findViewById(R.id.ETNombrePeludo);
        RazonPrincipal = (EditText) v.findViewById(R.id.editTextrazon);

        btnFecha.setOnClickListener(this);
        btnHora.setOnClickListener(this);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnFecha){
            final Calendar c = Calendar.getInstance();
            dia = c.get(Calendar.DAY_OF_MONTH);
            mes = c.get(Calendar.MONTH);
            anio = c.get(Calendar.YEAR);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    etFecha.setText(""+dayOfMonth+"/"+monthOfYear+"/"+year);
                }
            },anio, mes, dia);
            datePickerDialog.show();
        }
        if(v==btnHora){
            final Calendar c = Calendar.getInstance();
            hora = c.get(Calendar.HOUR);
            minuto = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    etHora.setText(""+returnFormatHour(hourOfDay,minute));
                }
            },hora, minuto,true);
            timePickerDialog.show();
        }
    }

    private String returnFormatHour(int hora, int minuto){
        String hr="", min="";
        if(hora < 10)
            hr = "0"+hora;
        else
            hr=""+hora;

        if(minuto<10)
            min = "0"+minuto;
        else
            min = ""+minuto;

        if(minuto==0)
            min = "00";
        if(hora==0)
            hr="00";

        return hr+":"+min;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
