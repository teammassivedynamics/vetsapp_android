package com.bishop.vetsapp40;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements OnNavigationItemSelectedListener,
        muestra_consultas.OnFragmentInteractionListener, muestra_pacientes.OnFragmentInteractionListener
,fragment_paciente.OnFragmentInteractionListener,Fragment_client.OnFragmentInteractionListener,
detalle_cliente.OnFragmentInteractionListener, detalle_paciente.OnFragmentInteractionListener,
detalles_gr_cliente.OnFragmentInteractionListener, cambiaDatos_cliente.OnFragmentInteractionListener,
notificaciones_generales.OnFragmentInteractionListener, busca_cliente.OnFragmentInteractionListener{

    public  Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.context = getApplicationContext();

        FragmentManager frMan = getSupportFragmentManager();
        frMan.beginTransaction().replace(R.id.Contenido_princ, new muestra_consultas()).commit();

        //-------------------Fin Dinamic Text view------

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Objects.requireNonNull(getCurrentFocus()).clearFocus();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        getMenuInflater().inflate(R.menu.main, menu);

        //View.clearFocus();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(MainActivity.this, Ajustes.class);
            startActivity(i);
            return true;
        }
        else if(id == R.id.action_cond_uso){
            Uri uri = Uri.parse("http://www.bishop-e.com");
            Intent inte =new Intent(Intent.ACTION_VIEW,uri);
            startActivity(inte);
            return true;
        }
        else if(id == R.id.action_politica_priv){
            Uri uri = Uri.parse("http://www.bishop-e.com");
            Intent inte =new Intent(Intent.ACTION_VIEW,uri);
            startActivity(inte);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        FragmentManager frMan = getSupportFragmentManager();
        if (id == R.id.nav_camera) {//Clientes
            frMan.beginTransaction().replace(R.id.Contenido_princ, new busca_cliente()).commit();
        } else if (id == R.id.nav_gallery) {//Pacientes
            frMan.beginTransaction().replace(R.id.Contenido_princ, new muestra_pacientes()).commit();
        } else if (id == R.id.nav_slideshow) {//Agendar cita
            frMan.beginTransaction().replace(R.id.Contenido_princ, new muestra_consultas()).commit();
        } else if (id == R.id.nav_manage) {//Reportes
            Toast.makeText(this, "Area de Reportes", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_share) {//Redes sclrociales
            Toast.makeText(this, "Area de Redes sociales", Toast.LENGTH_SHORT).show();
        }
        else if(id == R.id.nav_nuevo){
            frMan.beginTransaction().replace(R.id.Contenido_princ, new notificaciones_generales()).commit();
        }
        else if(id==R.id.nav_close){
            Preferences.savePreferenceBoolean(MainActivity.this,false, Preferences.PREFERENCE_ESTADO_BUTTON);
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public  void getAlert(String msg, String title){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setIcon(getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));
       alert.setButton(AlertDialog.BUTTON_POSITIVE, "Sí", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               Toast.makeText(getApplicationContext(),"Borrado",Toast.LENGTH_LONG).show();
           }
       });
        alert.show();

    }

}
