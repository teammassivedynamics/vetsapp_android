package com.bishop.vetsapp40.Adaptadores;

/**
 * Created by Javi Malikian on 12/09/2017.
 */

public class Items_cliente {
    private String Nombre;
    private int id;
    private String Direccion;
    private String Telefono;
    private String CorreoE;


    public Items_cliente(String nombre, int id, String Direccion, String Telefono, String Telefono2, String CorreoE) {
        this.Nombre = nombre;
        this.id = id;
        this.Direccion = Direccion;
        this.Telefono = Telefono;
        this.Telefono = Telefono2;
        this.CorreoE = CorreoE;
    }

    public String getDireccion() {return Direccion;}

    public void setDireccion(String direccion) {Direccion = direccion;}

    public String getTelefono() {return Telefono;}

    public void setTelefono(String telefono) {Telefono = telefono;}

    public String getCorreoE() {return CorreoE;}

    public void setCorreoE(String correoE) {CorreoE = correoE;}

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
