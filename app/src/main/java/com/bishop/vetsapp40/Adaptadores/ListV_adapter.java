package com.bishop.vetsapp40.Adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bishop.vetsapp40.R;

import java.util.ArrayList;

/**
 * Created by Javi Malikian on 29/08/2017.
 */

public class ListV_adapter extends BaseAdapter{

    public ArrayList<Items> arrayListItem;
    private Context context;
    private LayoutInflater layoutInflater;

    public ListV_adapter(Context context, ArrayList<Items> arrayListItem) {
        this.arrayListItem = arrayListItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayListItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vistaItem = layoutInflater.inflate(R.layout.lista_consultas_template,parent,false);
        //ImageView ivImagen = (ImageView) vistaItem.findViewById(R.id.idiVImg);
        TextView tvNombrePers = (TextView) vistaItem.findViewById(R.id.nombrePer);
        TextView tvNombrePet = (TextView) vistaItem.findViewById(R.id.nombrePet);
        TextView tvHora = (TextView) vistaItem.findViewById(R.id.horaConsulta);
        TextView tvRazon = (TextView) vistaItem.findViewById(R.id.razonConsulta);
        TextView tvLugar = (TextView) vistaItem.findViewById(R.id.lugarConsulta);

        //ivImagen.setImageResource(arrayListItem.get(position).getImagen());
        tvNombrePers.setText(arrayListItem.get(position).getNombre_persona());
        tvNombrePet.setText(arrayListItem.get(position).getNombre_pet());
        tvHora.setText(arrayListItem.get(position).getHora_consulta());
        tvRazon.setText(arrayListItem.get(position).getRazon_consulta());
        tvLugar.setText(arrayListItem.get(position).getLugar_consulta());

        //Metodo para redondeo de imagenes
        Drawable imgPerro = context.getResources().getDrawable(R.drawable.cutedog);
        Bitmap imgPerroBitmap = ((BitmapDrawable) imgPerro).getBitmap();
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imgPerroBitmap);
        roundedBitmapDrawable.setCornerRadius(imgPerroBitmap.getWidth());
        ImageView imageView= (ImageView) vistaItem.findViewById(R.id.idiVImg);
        imageView.setImageDrawable(roundedBitmapDrawable);

        return vistaItem;
    }
}
