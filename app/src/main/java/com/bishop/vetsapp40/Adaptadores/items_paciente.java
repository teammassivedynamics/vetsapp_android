package com.bishop.vetsapp40.Adaptadores;

import android.graphics.Path;

/**
 * Created by javim on 12/01/2018.
 */

public class items_paciente {
    private String Nombre_paciente;
    private int id_paciente;
    private int id_cliente;
    private int edad;
    private int sexo;
    private String cuadro_vacuna;
    private String Path_foto;

    public items_paciente(String Nombre_paciente, int id_paciente, int id_cliente, int edad, int sexo, String cuadro_vacuna, String Path_foto) {
        this.Nombre_paciente = Nombre_paciente;
        this.id_paciente = id_paciente;
        this.id_cliente = id_cliente;
        this.edad = edad;
        this.sexo = sexo;
        this.cuadro_vacuna = cuadro_vacuna;
        this.Path_foto = Path_foto;
    }

    public String getPath_foto() {
        return Path_foto;
    }

    public void setPath_foto(String path_foto) {
        Path_foto = path_foto;
    }

    public int getId_paciente() {
        return id_paciente;
    }

    public void setId_paciente(int id_paciente) {
        this.id_paciente = id_paciente;
    }

    public String getNombre_paciente() {
        return Nombre_paciente;
    }

    public void setNombre_paciente(String nombre_paciente) {
        Nombre_paciente = nombre_paciente;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public String getCuadro_vacuna() {
        return cuadro_vacuna;
    }

    public void setCuadro_vacuna(String cuadro_vacuna) {
        this.cuadro_vacuna = cuadro_vacuna;
    }



}
