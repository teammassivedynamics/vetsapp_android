package com.bishop.vetsapp40.Adaptadores;

/**
 * Created by Javi Malikian on 29/08/2017.
 */

public class Items {

    private int imagen;
    private String id_cliente;
    private String nombre_persona;
    private String nombre_pet;
    private String hora_consulta;
    private String razon_consulta;
    private String lugar_consulta;
    private String fecha_consulta;
    private String mensaje;



    public Items(String id_cliente, int imagen, String nombre_persona, String nombre_pet, String hora_consulta, String razon_consulta, String lugar_consulta, String fecha_consulta, String mensaje) {
        this.id_cliente = id_cliente;
        this.imagen = imagen;
        this.nombre_persona = nombre_persona;
        this.nombre_pet = nombre_pet;
        this.hora_consulta = hora_consulta;
        this.razon_consulta = razon_consulta;
        this.lugar_consulta = lugar_consulta;
        this.fecha_consulta = fecha_consulta;
        this.mensaje = mensaje;
    }
    public String getMensaje() {return mensaje;}

    public void setMensaje(String mensaje) {this.mensaje = mensaje;}

    public String getFecha_consulta() {
        return fecha_consulta;
    }

    public void setFecha_consulta(String fecha_consulta) {
        this.fecha_consulta = fecha_consulta;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre_persona() {
        return nombre_persona;
    }

    public void setNombre_persona(String nombre_persona) {
        this.nombre_persona = nombre_persona;
    }

    public String getNombre_pet() {
        return nombre_pet;
    }

    public void setNombre_pet(String nombre_pet) {
        this.nombre_pet = nombre_pet;
    }

    public String getHora_consulta() {
        return hora_consulta;
    }

    public void setHora_consulta(String hora_consulta) {
        this.hora_consulta = hora_consulta;
    }

    public String getRazon_consulta() {
        return razon_consulta;
    }

    public void setRazon_consulta(String razon_consulta) {
        this.razon_consulta = razon_consulta;
    }

    public String getLugar_consulta() {
        return lugar_consulta;
    }

    public void setLugar_consulta(String lugar_consulta) {
        this.lugar_consulta = lugar_consulta;
    }
}
