package com.bishop.vetsapp40.Adaptadores;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bishop.vetsapp40.R;

import java.util.ArrayList;

/**
 * Created by javim on 12/01/2018.
 */

public class ListViewAdapter_pacientes extends BaseAdapter {
    public ArrayList<items_paciente> arrarListItem;
    private Context context;
    private LayoutInflater layoutinflater;
    private View v;

    public ListViewAdapter_pacientes(ArrayList<items_paciente> arrarListItem, Context context){
        this.arrarListItem = arrarListItem;
        this.context = context;
    }

    @Override
    public int getCount() {return  arrarListItem.size();    }

    @Override
    public Object getItem(int position) {return  arrarListItem.get(position);}

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutinflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View vistaItem = layoutinflater.inflate(R.layout.lista_pacientes_template,parent, false);
        final TextView tvIdPaciente =(TextView)vistaItem.findViewById(R.id.id_paciente);
        final TextView tvIdCliente =(TextView)vistaItem.findViewById(R.id.id_cliente);
        final TextView tvNombrePaciente =(TextView)vistaItem.findViewById(R.id.nombrePaciente);
        final TextView tvEdadPaciente =(TextView)vistaItem.findViewById(R.id.edad_paciente);
        final TextView tvSexoPaciente =(TextView)vistaItem.findViewById(R.id.sexo);

        tvIdPaciente.setText(""+arrarListItem.get(position).getId_paciente());
        tvIdCliente.setText(""+arrarListItem.get(position).getId_cliente());
        tvNombrePaciente.setText(""+arrarListItem.get(position).getNombre_paciente());
        tvEdadPaciente.setText(arrarListItem.get(position).getEdad()+" años");

        Drawable imgPerro = context.getResources().getDrawable(R.drawable.perro);
        Bitmap imgPerroBitmap = ((BitmapDrawable) imgPerro).getBitmap();
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), imgPerroBitmap);
        roundedBitmapDrawable.setCornerRadius(imgPerroBitmap.getHeight());
        ImageView imageView= (ImageView) vistaItem.findViewById(R.id.img_perro);
        imageView.setImageDrawable(roundedBitmapDrawable);


        String sex="";
        if(arrarListItem.get(position).getSexo() == 1)
            sex = "Macho";
        else
            sex ="Hembra";


        tvSexoPaciente.setText(sex);




        return vistaItem;
    }
}
