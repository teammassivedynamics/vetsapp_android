package com.bishop.vetsapp40.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bishop.vetsapp40.R;

import java.util.ArrayList;

/**
 * Created by Javi Malikian on 18/09/2017.
 */

public class ListViewAdapter_generos extends BaseAdapter {
    public ArrayList<Items_generos> arrarListItem;
    private Context context;
    private LayoutInflater layoutinflater;

    public ListViewAdapter_generos(ArrayList<Items_generos> arrarListItem, Context context){
        this.arrarListItem = arrarListItem;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrarListItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arrarListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutinflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View vistaItem = layoutinflater.inflate(R.layout.lista_generos_template,parent, false);
        final TextView tvNomb_genero = (TextView)vistaItem.findViewById(R.id.nombreGenero);
        final TextView tvId = (TextView)vistaItem.findViewById(R.id.id);

        tvNomb_genero.setText(arrarListItem.get(position).getNombre());
        tvId.setText(arrarListItem.get(position).getId());

        return vistaItem;
    }
}
