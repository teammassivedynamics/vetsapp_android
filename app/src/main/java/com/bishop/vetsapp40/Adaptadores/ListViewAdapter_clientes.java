package com.bishop.vetsapp40.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bishop.vetsapp40.R;

import java.util.ArrayList;

/**
 * Created by Javi Malikian on 12/09/2017.
 */

public class ListViewAdapter_clientes extends BaseAdapter {

    public ArrayList<Items_cliente> arrarListItem;
    private Context context;
    private LayoutInflater layoutinflater;
    private View v;



    public ListViewAdapter_clientes(ArrayList<Items_cliente> arrarListItem, Context context) {
        this.arrarListItem = arrarListItem;
        this.context = context;
    }


    @Override
    public int getCount() {
        return  arrarListItem.size();
    }

    @Override
    public Object getItem(int position) {
        return  arrarListItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        layoutinflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View vistaItem = layoutinflater.inflate(R.layout.lista_clientes_template,parent, false);
        final TextView tvNombre =(TextView)vistaItem.findViewById(R.id.nombrePer_);
        final TextView tvId =(TextView)vistaItem.findViewById(R.id.id);
        final TextView tvDireccion = (TextView)vistaItem.findViewById(R.id.direccion);
        final TextView tvTelefono = (TextView)vistaItem.findViewById(R.id.telefono);

        tvNombre.setText(arrarListItem.get(position).getNombre());
        tvId.setText(""+arrarListItem.get(position).getId());
        tvDireccion.setText("Direccion: "+arrarListItem.get(position).getDireccion());
        tvTelefono.setText("Teléfono: "+arrarListItem.get(position).getTelefono());

        return vistaItem;
    }
}
