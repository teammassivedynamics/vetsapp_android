package com.bishop.vetsapp40.Adaptadores;

/**
 * Created by Javi Malikian on 18/09/2017.
 */

public class Items_generos {
    private String  Nombre;
    private String id;

    public Items_generos(String nombre, String id) {
        Nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
