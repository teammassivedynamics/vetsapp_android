package com.bishop.vetsapp40;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.bishop.vetsapp40.Adaptadores.ListViewAdapter_pacientes;
import com.bishop.vetsapp40.Adaptadores.items_paciente;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link muestra_pacientes.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link muestra_pacientes#newInstance} factory method to
 * create an instance of this fragment.
 */
public class muestra_pacientes extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private ListViewAdapter_pacientes adapter = null;
    private ListView ListaPersonalizada = null;
    private ArrayList<items_paciente> arrayItems = null;
    View v;

    //IP de mi Url
    String IP ="http://vetsapp20180130022001.azurewebsites.net/Customer";

    //Ruta del web service
    String GET_PACIENTES= IP +"/getMedicalConsultationList";

    ObtenerWebService hiloConexion;

    private OnFragmentInteractionListener mListener;

    public muestra_pacientes() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment muestra_pacientes.
     */
    // TODO: Rename and change types and number of parameters
    public static muestra_pacientes newInstance(String param1, String param2) {
        muestra_pacientes fragment = new muestra_pacientes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v=  inflater.inflate(R.layout.fragment_muestra_pacientes, container, false);
        Button btnMuestraHistorial = (Button) v.findViewById(R.id.btnVerHistorial);
        setHasOptionsMenu(true);
        ListaPersonalizada = (ListView) v.findViewById(R.id.ListVItemsPacientes);
        arrayItems = new ArrayList<>();
        cargarLista();

        registerForContextMenu(ListaPersonalizada);
        final FragmentManager frMan = getActivity().getSupportFragmentManager();

        ListaPersonalizada.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                frMan.beginTransaction().replace(R.id.Contenido_princ, new detalle_paciente()).commit();
            }
        });


        return v;
    }

    private void cargarLista() {
        //String Nombre_paciente, int id_paciente, int id_cliente, int edad, String razgos_dist, int sexo, String cuadro_vacuna

        arrayItems.add(new items_paciente("Rayo", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Akane", 1,1, 3, 1, "cuadro completo",""));
        arrayItems.add(new items_paciente("Tomy", 1,1, 3, 1, "cuadro completo",""));
        arrayItems.add(new items_paciente("Bruse", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Remy", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Pantunfla", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Tedy", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Colon", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Canelon", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Pury", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("De la cuadra", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("José Luis", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Nigga", 1,1, 3, 1, "cuadro completo", ""));
        arrayItems.add(new items_paciente("Pepe", 1,1, 3, 1, "cuadro completo", ""));


        adapter = new ListViewAdapter_pacientes (arrayItems, this.getActivity());
        ListaPersonalizada.setAdapter(adapter);
    }

    private void cargaLista2(){
        hiloConexion =new ObtenerWebService();
        hiloConexion.execute(GET_PACIENTES,"1");

    }

    public void getAlert(String msg, String title){
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setIcon(v.getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));

        alert.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "Borrado",Toast.LENGTH_LONG).show();

            }

        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "No",Toast.LENGTH_LONG).show();
            }
        });
        alert.show();
    }

    //Menu en el action bar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_add_paciente, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager frMan = getActivity().getSupportFragmentManager();

        switch (item.getItemId()){
            case R.id.addPaciente_pet:
                frMan.beginTransaction().replace(R.id.Contenido_princ, new fragment_paciente()).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Menu flotante para las consultas
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrarPaciente:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrarListItem.get(info.position).getNombre_paciente(),Toast.LENGTH_SHORT).show();
                getAlert("¿Desea eliminar el registro "+adapter.arrarListItem.get(info.position).getNombre_paciente()+"?", "Alerta Borrar");
                return true;
            case R.id.modificarPaciente:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrarListItem.get(info.position).getNombre_paciente(),Toast.LENGTH_SHORT).show();
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(""+adapter.arrarListItem.get(info.position).getNombre_paciente());
        inflater.inflate(R.menu.menu_float_paciente, menu);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public class ObtenerWebService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }
                        String result1 = result.toString();
                        result1 = result1.replace('\\', '"');
                        //result1 = result1.replace('/', ' ' );

                        JSONObject respuestaJSON = new JSONObject(result1);
                        String resultJSON = respuestaJSON.getString("Status");

                        if(resultJSON.equals("1")){
                            //datosConsulta = resultJSON.getJSONObject(0).getJSONArray("consultas");
                            JSONArray consultaJSON = respuestaJSON.getJSONArray("JsonResults");

                            // JSONArray JsonsResults = respuestaJSON.getJSONArray("JsonResults");
                            //JSONArray consultaJSON = JsonsResults.getJSONArray(0);

                            for (int i=0;i<consultaJSON.length();i++){
                                arrayItems.add(new items_paciente(
                                        consultaJSON.getJSONObject(i).getString("IdCustomer"),
                                        consultaJSON.getJSONObject(i).getInt("CustomerName"),
                                        consultaJSON.getJSONObject(i).getInt("PeludoName"),
                                        consultaJSON.getJSONObject(i).getInt("ConsultationDateAndHour"),
                                        consultaJSON.getJSONObject(i).getInt("Description"),
                                        consultaJSON.getJSONObject(i).getString("RegistrationDate"),
                                        consultaJSON.getJSONObject(i).getString("AditionalInformation"))
                                );

                                adapter = new ListViewAdapter_pacientes( arrayItems,getActivity());
                                ListaPersonalizada.setAdapter(adapter);
                               /* devuelve = devuelve+consultaJSON.getJSONObject(i).getString("id_persona")+" "+
                                        consultaJSON.getJSONObject(i).getString("nombre_persona")+" "+
                                        consultaJSON.getJSONObject(i).getString("nombre_pet")+" "+
                                        consultaJSON.getJSONObject(i).getString("hora_consulta")+" "+
                                        consultaJSON.getJSONObject(i).getString("razon_consulta")+" "+
                                        consultaJSON.getJSONObject(i).getString("lugar_consulta")+" \n";*/
                            }
                        }

                        devuelve = respuestaJSON.getString("Message");;


                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return devuelve;
        }
    }
}
