package com.bishop.vetsapp40;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;


import com.bishop.vetsapp40.Adaptadores.Items;
import com.bishop.vetsapp40.Adaptadores.ListV_adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link muestra_consultas.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link muestra_consultas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class muestra_consultas extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    LinearLayout Conttenedor;
    private ListV_adapter adapter = null;
    private ListView ListaPersonalizada = null;
    private ArrayList<Items> arrayItems = null;
    View v;

    //IP de mi Url
    String IP ="http://vetsapp20180130022001.azurewebsites.net/Customer";

    //Ruta del web service
    String GET_CONSULTAS= IP +"/getMedicalConsultationList";

    ObtenerWebService hiloConexion;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public muestra_consultas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment muestra_consultas.
     */
    // TODO: Rename and change types and number of parameters
    public static muestra_consultas newInstance(String param1, String param2) {
        muestra_consultas fragment = new muestra_consultas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_muestra_consultas, container, false);
        setHasOptionsMenu(true);
        ListaPersonalizada = (ListView) v.findViewById(R.id.ListVItems);
        arrayItems = new ArrayList<>();
        cargaLista2();

        ListaPersonalizada.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return true;
            }
        });

        ListaPersonalizada.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Es: "+adapter.arrayListItem.get(position).getNombre_persona(), Toast.LENGTH_SHORT).show();
            }
        });

        registerForContextMenu(ListaPersonalizada);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }


    }

    private void cargarLista() {
        //Nombre persona, nombre mascota, hora, razón, lugar

        arrayItems.add(new Items("1",R.drawable.cutedog, "Javier Castro", "Tommy", "10:00 pm", "Vacuna", "Av escutia, 19, El paraíso", "12/01/18", "mensaje"));
        arrayItems.add(new Items("1",R.drawable.perro, "Victor Castro", "Remmy", "12:50 pm", "Desparasitacióm", "uAv escutia, 19, El paraíso", "12/01/18", "mensaje"));
        arrayItems.add(new Items("1",R.drawable.perro, "Esmeralda cortes", "Akane", "1:40 pm", "Cirugia", "Av panza, 19, El paraíso", "12/01/18", "mensaje"));
        arrayItems.add(new Items("1",R.drawable.perro, "Nidia Robles", "Tedy", "9:00 am", "Cirugía ernia", "Av pelona, 19, El paraíso, zacatecas", "12/01/18", "mensaje"));
        arrayItems.add(new Items("1",R.drawable.perro, "Gerardo Reyes", "Tola", "6:20 pm", "Corte pelo", "Av la chismosa, 19, El paraíso", "12/01/18", "mensaje"));
        arrayItems.add(new Items("1",R.drawable.perro, "Francisca COrtes", "Chola", "7:20 pm", "Corte orejas", "Av la vata, 19, El paraíso", "12/01/18", "mensaje"));

        adapter = new ListV_adapter(this.getActivity().getApplicationContext(), arrayItems);
        ListaPersonalizada.setAdapter(adapter);
    }

    private void cargaLista2(){
        hiloConexion =new ObtenerWebService();
        hiloConexion.execute(GET_CONSULTAS,"1");
    }


    public void onAttach(Context context) {

        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void getAlert(String msg, String title){
        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setIcon(v.getResources().getDrawable(R.drawable.ic_delete_forever_black_24dp));

        alert.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "Borrado",Toast.LENGTH_LONG).show();

            }

        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(v.getContext().getApplicationContext(), "No",Toast.LENGTH_LONG).show();
            }
        });
        alert.show();
    }


    //Menu superior en el action bar

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_add_consult, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentManager frMan = getActivity().getSupportFragmentManager();

        switch (item.getItemId()){
            case R.id.addConsulta:
                frMan.beginTransaction().replace(R.id.Contenido_princ, new agen_cita()).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //Menu flotante para las consultas

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){
            case R.id.borrar:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrayListItem.get(info.position).getNombre_persona(),Toast.LENGTH_SHORT).show();
                getAlert("¿Desea concluir esta consulta de "+adapter.arrayListItem.get(info.position).getNombre_persona()+"?", "Alerta Borrar");
                return true;
            case R.id.modificar:
                Toast.makeText(getActivity().getApplication(),"Es: "+adapter.arrayListItem.get(info.position).getNombre_persona(),Toast.LENGTH_SHORT).show();
                return true;
            default:return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getActivity().getMenuInflater();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(""+adapter.arrayListItem.get(info.position).getNombre_persona());
        inflater.inflate(R.menu.menu_float_consultas, menu);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public class ObtenerWebService extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {

            Toast.makeText(getActivity(), "Cargando, espere...", Toast.LENGTH_SHORT).show();
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

            Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
            adapter = new ListV_adapter(getActivity(), arrayItems);
            ListaPersonalizada.setAdapter(adapter);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                    "(Linux; Android 1.5; es-ES) Ejemplo HTTP");
                    connection.setRequestMethod("GET");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }
                        String json = "{\"Id\":0,\"Message\":\"Lista de consultas incluidas con exito\",\"Status\":\"1\",\"JsonResults\":[{\"IdConsultation\":0,\"IdCustomer\":100,\"CustomerName\":\"Nombre 0\",\"PeludoName\":\"Perro 0\",\"Description\":\"Esta es la descripcion del perro 0\",\"ConsultationPlace\":\"Calle 0 avenida 0 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 0 pulgas\",\"RegistrationDate\":\"\\/Date(1517524127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517524127514)\\/\"},{\"IdConsultation\":1,\"IdCustomer\":101,\"CustomerName\":\"Nombre 1\",\"PeludoName\":\"Cerdito 1\",\"Description\":\"Esta es la descripcion del perro 1\",\"ConsultationPlace\":\"Calle 2 avenida 3 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 1 pulgas\",\"RegistrationDate\":\"\\/Date(1517351327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517696927514)\\/\"},{\"IdConsultation\":2,\"IdCustomer\":102,\"CustomerName\":\"Nombre 2\",\"PeludoName\":\"Cerdito 2\",\"Description\":\"Esta es la descripcion del perro 2\",\"ConsultationPlace\":\"Calle 4 avenida 6 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 2 pulgas\",\"RegistrationDate\":\"\\/Date(1517178527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517869727514)\\/\"},{\"IdConsultation\":3,\"IdCustomer\":103,\"CustomerName\":\"Nombre 3\",\"PeludoName\":\"Gato 3\",\"Description\":\"Esta es la descripcion del perro 3\",\"ConsultationPlace\":\"Calle 6 avenida 9 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 3 pulgas\",\"RegistrationDate\":\"\\/Date(1517005727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518042527514)\\/\"},{\"IdConsultation\":4,\"IdCustomer\":104,\"CustomerName\":\"Nombre 4\",\"PeludoName\":\"Perro 4\",\"Description\":\"Esta es la descripcion del perro 4\",\"ConsultationPlace\":\"Calle 8 avenida 12 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 4 pulgas\",\"RegistrationDate\":\"\\/Date(1516832927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518215327514)\\/\"},{\"IdConsultation\":5,\"IdCustomer\":105,\"CustomerName\":\"Nombre 5\",\"PeludoName\":\"Cerdito 5\",\"Description\":\"Esta es la descripcion del perro 5\",\"ConsultationPlace\":\"Calle 10 avenida 15 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 5 pulgas\",\"RegistrationDate\":\"\\/Date(1516660127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518388127514)\\/\"},{\"IdConsultation\":6,\"IdCustomer\":106,\"CustomerName\":\"Nombre 6\",\"PeludoName\":\"Gato 6\",\"Description\":\"Esta es la descripcion del perro 6\",\"ConsultationPlace\":\"Calle 12 avenida 18 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 6 pulgas\",\"RegistrationDate\":\"\\/Date(1516487327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518560927514)\\/\"},{\"IdConsultation\":7,\"IdCustomer\":107,\"CustomerName\":\"Nombre 7\",\"PeludoName\":\"Cerdito 7\",\"Description\":\"Esta es la descripcion del perro 7\",\"ConsultationPlace\":\"Calle 14 avenida 21 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 7 pulgas\",\"RegistrationDate\":\"\\/Date(1516314527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518733727514)\\/\"},{\"IdConsultation\":8,\"IdCustomer\":108,\"CustomerName\":\"Nombre 8\",\"PeludoName\":\"Perro 8\",\"Description\":\"Esta es la descripcion del perro 8\",\"ConsultationPlace\":\"Calle 16 avenida 24 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 8 pulgas\",\"RegistrationDate\":\"\\/Date(1516141727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518906527514)\\/\"},{\"IdConsultation\":9,\"IdCustomer\":109,\"CustomerName\":\"Nombre 9\",\"PeludoName\":\"Gato 9\",\"Description\":\"Esta es la descripcion del perro 9\",\"ConsultationPlace\":\"Calle 18 avenida 27 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 9 pulgas\",\"RegistrationDate\":\"\\/Date(1515968927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519079327514)\\/\"},{\"IdConsultation\":10,\"IdCustomer\":110,\"CustomerName\":\"Nombre 10\",\"PeludoName\":\"Cerdito 10\",\"Description\":\"Esta es la descripcion del perro 10\",\"ConsultationPlace\":\"Calle 20 avenida 30 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 10 pulgas\",\"RegistrationDate\":\"\\/Date(1515796127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519252127514)\\/\"},{\"IdConsultation\":11,\"IdCustomer\":111,\"CustomerName\":\"Nombre 11\",\"PeludoName\":\"Cerdito 11\",\"Description\":\"Esta es la descripcion del perro 11\",\"ConsultationPlace\":\"Calle 22 avenida 33 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 11 pulgas\",\"RegistrationDate\":\"\\/Date(1515623327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519424927514)\\/\"},{\"IdConsultation\":12,\"IdCustomer\":112,\"CustomerName\":\"Nombre 12\",\"PeludoName\":\"Perro 12\",\"Description\":\"Esta es la descripcion del perro 12\",\"ConsultationPlace\":\"Calle 24 avenida 36 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 12 pulgas\",\"RegistrationDate\":\"\\/Date(1515450527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519597727514)\\/\"},{\"IdConsultation\":13,\"IdCustomer\":113,\"CustomerName\":\"Nombre 13\",\"PeludoName\":\"Cerdito 13\",\"Description\":\"Esta es la descripcion del perro 13\",\"ConsultationPlace\":\"Calle 26 avenida 39 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 13 pulgas\",\"RegistrationDate\":\"\\/Date(1515277727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519770527514)\\/\"},{\"IdConsultation\":14,\"IdCustomer\":114,\"CustomerName\":\"Nombre 14\",\"PeludoName\":\"Cerdito 14\",\"Description\":\"Esta es la descripcion del perro 14\",\"ConsultationPlace\":\"Calle 28 avenida 42 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 14 pulgas\",\"RegistrationDate\":\"\\/Date(1515104927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519943327514)\\/\"},{\"IdConsultation\":15,\"IdCustomer\":115,\"CustomerName\":\"Nombre 15\",\"PeludoName\":\"Gato 15\",\"Description\":\"Esta es la descripcion del perro 15\",\"ConsultationPlace\":\"Calle 30 avenida 45 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 15 pulgas\",\"RegistrationDate\":\"\\/Date(1514932127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520116127514)\\/\"},{\"IdConsultation\":16,\"IdCustomer\":116,\"CustomerName\":\"Nombre 16\",\"PeludoName\":\"Perro 16\",\"Description\":\"Esta es la descripcion del perro 16\",\"ConsultationPlace\":\"Calle 32 avenida 48 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 16 pulgas\",\"RegistrationDate\":\"\\/Date(1514759327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520288927514)\\/\"},{\"IdConsultation\":17,\"IdCustomer\":117,\"CustomerName\":\"Nombre 17\",\"PeludoName\":\"Cerdito 17\",\"Description\":\"Esta es la descripcion del perro 17\",\"ConsultationPlace\":\"Calle 34 avenida 51 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 17 pulgas\",\"RegistrationDate\":\"\\/Date(1514586527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520461727514)\\/\"},{\"IdConsultation\":18,\"IdCustomer\":118,\"CustomerName\":\"Nombre 18\",\"PeludoName\":\"Gato 18\",\"Description\":\"Esta es la descripcion del perro 18\",\"ConsultationPlace\":\"Calle 36 avenida 54 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 18 pulgas\",\"RegistrationDate\":\"\\/Date(1514413727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520634527514)\\/\"},{\"IdConsultation\":19,\"IdCustomer\":119,\"CustomerName\":\"Nombre 19\",\"PeludoName\":\"Cerdito 19\",\"Description\":\"Esta es la descripcion del perro 19\",\"ConsultationPlace\":\"Calle 38 avenida 57 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 19 pulgas\",\"RegistrationDate\":\"\\/Date(1514240927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520807327514)\\/\"}]}";

                        JSONObject respuestaJSON = new JSONObject(json);
                       // JSONObject respuestaJSON = new JSONObject(result.toString());
                        String resultJSON = respuestaJSON.getString("Status");

                        if(resultJSON.equals("1")){
                            JSONArray consultaJSON = respuestaJSON.getJSONArray("JsonResults");

                            for (int i=0;i < consultaJSON.length();i++){
                                arrayItems.add(new Items(
                                        consultaJSON.getJSONObject(i).getInt("IdCustomer")+"",
                                        R.drawable.cutedog,
                                        consultaJSON.getJSONObject(i).getString("CustomerName"),
                                        consultaJSON.getJSONObject(i).getString("PeludoName"),
                                        consultaJSON.getJSONObject(i).getString("ConsultationDateAndHour"),
                                        consultaJSON.getJSONObject(i).getString("Description"),
                                        consultaJSON.getJSONObject(i).getString("ConsultationPlace"),
                                        consultaJSON.getJSONObject(i).getString("RegistrationDate"),
                                        consultaJSON.getJSONObject(i).getString("AditionalInformation"))
                                );
                            }
                        }


                        devuelve = respuestaJSON.getString("Message");;
                    }
                    else
                        devuelve = "Revisa la conexion a internet e inténtalo nuevamente";
                }
                 catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            return devuelve;
        }
    }
}
