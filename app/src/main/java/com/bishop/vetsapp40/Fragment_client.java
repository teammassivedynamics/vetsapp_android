package com.bishop.vetsapp40;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragment_client.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Fragment_client#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_client extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String Snombre;
    private  String Sapels;
    private String Stel1;
    private String Stel2;
    private String Sdireccion;
    private String Sciudad;
    private String Semail;

    private Button btnGuardaDatos;

    private String IP = "http://vetsapp20180130022001.azurewebsites.net/Customer";
    private String SET_CLIENTE = "/SetNewClient?";
    ObtenerWebService hiloConexion;


    private OnFragmentInteractionListener mListener;

    public Fragment_client() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment_client.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_client newInstance(String param1, String param2) {
        Fragment_client fragment = new Fragment_client();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_fragment_client, container, false);
        Snombre = ((EditText)rootView.findViewById(R.id.editTextnombreCliente)).getText().toString();
        Sapels = ((EditText)rootView.findViewById(R.id.editTextApels)).getText().toString();
        Stel1 = ((EditText)rootView.findViewById(R.id.editTextTel)).getText().toString();
        Stel2 = ((EditText)rootView.findViewById(R.id.editTextTel2)).getText().toString();
        Sdireccion = ((EditText)rootView.findViewById(R.id.editTextDir)).getText().toString();
        Sciudad = ((EditText)rootView.findViewById(R.id.editTextCiudad)).getText().toString();
        Semail = ((EditText)rootView.findViewById(R.id.editTextCorreo)).getText().toString();

        btnGuardaDatos = (Button) rootView.findViewById(R.id.btnGuardaDatos);

        btnGuardaDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Snombre.length()>0){
                    Toast.makeText(getActivity(), "El nombre no puede estar vacío", Toast.LENGTH_LONG).show();
                }
                else if(Sapels.length()>0){
                    Toast.makeText(getActivity(), "Los Apellidos no pueden estar vacíos", Toast.LENGTH_LONG).show();
                }
                else if(Stel1.length()>0){
                    Toast.makeText(getActivity(), "El Teléfono principal no puede estar vacío", Toast.LENGTH_LONG).show();
                }
                else if(Sdireccion.length()>0){
                    Toast.makeText(getActivity(), "La direccion no puede estar vacía", Toast.LENGTH_LONG).show();
                }
                else if(Sciudad.length()>10){
                    Toast.makeText(getActivity(), "La ciudad no puede estar vacía", Toast.LENGTH_LONG).show();
                }
                else if(Semail.length()>10){
                    Toast.makeText(getActivity(), "El correo no puede estar vacío", Toast.LENGTH_LONG).show();
                }
                else{
                    SET_CLIENTE =IP + SET_CLIENTE+"nameClient="+Snombre+"?apelsClient="+Sapels+
                            "?tel1="+Stel1+"?tel2="+Stel2+"?adressClient="+Sdireccion+
                            "?cityClient="+Sciudad+"?emailClient="+Semail;

                    hiloConexion =new ObtenerWebService();
                    hiloConexion.execute(SET_CLIENTE,"1");
                }
            }
        });







        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public class ObtenerWebService extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            Toast.makeText(getActivity(), "Es: "+s, Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url;
            String devuelve="";

            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());
                        String resultJSON = respuestaJSON.getString("Status");

                        if(resultJSON.equals("1")){
                            respuestaJSON.getJSONArray("Message");
                            //devuelve = respuestaJSON.getString("Message");
                        }
                        devuelve = respuestaJSON.getString("Message");;
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return devuelve;
        }
    }
}
