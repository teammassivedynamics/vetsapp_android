package com.bishop.vetsapp40;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bishop.vetsapp40.Adaptadores.Items;
import com.bishop.vetsapp40.Adaptadores.ListV_adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.bishop.vetsapp40.R.id.textView;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    public CheckBox cBNoCerrar;
    private String ResultLogin;

    //IP de mi Url
    String IP ="http://vetsapp20180130022001.azurewebsites.net/Login";

    //Ruta del web service
    String GET_PROFILE_PERMISSION= IP +"/getResultUserAcount";

    ObtenerWebService hiloConexion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(Preferences.obtenerPreferenceBoolean(this,Preferences.PREFERENCE_ESTADO_BUTTON)){
            Intent sig = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(sig);
            finish();
        }

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.email || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        TextView butTexV = (TextView) findViewById(R.id.btnContra);
        butTexV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sig = new Intent(LoginActivity.this, recupera_contrasenia.class);
                startActivity(sig);
            }
        });

        cBNoCerrar = (CheckBox)findViewById(R.id.CbnOcerrar);
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_field_num));
            focusView = mEmailView;
            cancel = true;
        }

        hiloConexion =new ObtenerWebService();
        hiloConexion.execute(GET_PROFILE_PERMISSION,"1");

        if (cancel && !Objects.equals(ResultLogin, "1")) {
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Preferences.savePreferenceBoolean(LoginActivity.this,cBNoCerrar.isChecked(),Preferences.PREFERENCE_ESTADO_BUTTON);
            String emailw = mEmailView.getText().toString();
            Preferences.savePreferenceString(LoginActivity.this,emailw,Preferences.PREFERENCE_USUARIO_LOGIN);
            showProgress(true);
            Intent sig = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(sig);
            //finish();

           // mAuthTask = new UserLoginTask(email, password);
            //mAuthTask.execute((Void) null);

        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        boolean band = true;

        if(email.length()==2)
            band = true;
        else
            band = false;

        return band;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 2;
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                return false;
            }

            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    //Web service
    public class ObtenerWebService extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
           // adapter = new ListV_adapter(getActivity(), arrayItems);
            //ListaPersonalizada.setAdapter(adapter);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @TargetApi(Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... strings) {
            String cadena =strings[0];
            URL url=null;
            String devuelve="";
            String datosConsulta="";
            if(Objects.equals(strings[1], "1")){
                try{
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0"+
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");
                    connection.setRequestMethod("GET");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if(respuesta==HttpURLConnection.HTTP_OK){//200 ok 403 para no conexin
                        InputStream in = new BufferedInputStream(connection.getInputStream());
                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                        String line;
                        while ((line = reader.readLine())!= null){
                            result.append(line);
                        }
                        String json = "{\"Id\":0,\"Message\":\"Lista de consultas incluidas con exito\",\"Status\":\"1\",\"JsonResults\":[{\"IdConsultation\":0,\"IdCustomer\":100,\"CustomerName\":\"Nombre 0\",\"PeludoName\":\"Perro 0\",\"Description\":\"Esta es la descripcion del perro 0\",\"ConsultationPlace\":\"Calle 0 avenida 0 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 0 pulgas\",\"RegistrationDate\":\"\\/Date(1517524127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517524127514)\\/\"},{\"IdConsultation\":1,\"IdCustomer\":101,\"CustomerName\":\"Nombre 1\",\"PeludoName\":\"Cerdito 1\",\"Description\":\"Esta es la descripcion del perro 1\",\"ConsultationPlace\":\"Calle 2 avenida 3 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 1 pulgas\",\"RegistrationDate\":\"\\/Date(1517351327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517696927514)\\/\"},{\"IdConsultation\":2,\"IdCustomer\":102,\"CustomerName\":\"Nombre 2\",\"PeludoName\":\"Cerdito 2\",\"Description\":\"Esta es la descripcion del perro 2\",\"ConsultationPlace\":\"Calle 4 avenida 6 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 2 pulgas\",\"RegistrationDate\":\"\\/Date(1517178527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1517869727514)\\/\"},{\"IdConsultation\":3,\"IdCustomer\":103,\"CustomerName\":\"Nombre 3\",\"PeludoName\":\"Gato 3\",\"Description\":\"Esta es la descripcion del perro 3\",\"ConsultationPlace\":\"Calle 6 avenida 9 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 3 pulgas\",\"RegistrationDate\":\"\\/Date(1517005727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518042527514)\\/\"},{\"IdConsultation\":4,\"IdCustomer\":104,\"CustomerName\":\"Nombre 4\",\"PeludoName\":\"Perro 4\",\"Description\":\"Esta es la descripcion del perro 4\",\"ConsultationPlace\":\"Calle 8 avenida 12 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 4 pulgas\",\"RegistrationDate\":\"\\/Date(1516832927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518215327514)\\/\"},{\"IdConsultation\":5,\"IdCustomer\":105,\"CustomerName\":\"Nombre 5\",\"PeludoName\":\"Cerdito 5\",\"Description\":\"Esta es la descripcion del perro 5\",\"ConsultationPlace\":\"Calle 10 avenida 15 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 5 pulgas\",\"RegistrationDate\":\"\\/Date(1516660127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518388127514)\\/\"},{\"IdConsultation\":6,\"IdCustomer\":106,\"CustomerName\":\"Nombre 6\",\"PeludoName\":\"Gato 6\",\"Description\":\"Esta es la descripcion del perro 6\",\"ConsultationPlace\":\"Calle 12 avenida 18 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 6 pulgas\",\"RegistrationDate\":\"\\/Date(1516487327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518560927514)\\/\"},{\"IdConsultation\":7,\"IdCustomer\":107,\"CustomerName\":\"Nombre 7\",\"PeludoName\":\"Cerdito 7\",\"Description\":\"Esta es la descripcion del perro 7\",\"ConsultationPlace\":\"Calle 14 avenida 21 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 7 pulgas\",\"RegistrationDate\":\"\\/Date(1516314527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518733727514)\\/\"},{\"IdConsultation\":8,\"IdCustomer\":108,\"CustomerName\":\"Nombre 8\",\"PeludoName\":\"Perro 8\",\"Description\":\"Esta es la descripcion del perro 8\",\"ConsultationPlace\":\"Calle 16 avenida 24 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 8 pulgas\",\"RegistrationDate\":\"\\/Date(1516141727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1518906527514)\\/\"},{\"IdConsultation\":9,\"IdCustomer\":109,\"CustomerName\":\"Nombre 9\",\"PeludoName\":\"Gato 9\",\"Description\":\"Esta es la descripcion del perro 9\",\"ConsultationPlace\":\"Calle 18 avenida 27 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 9 pulgas\",\"RegistrationDate\":\"\\/Date(1515968927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519079327514)\\/\"},{\"IdConsultation\":10,\"IdCustomer\":110,\"CustomerName\":\"Nombre 10\",\"PeludoName\":\"Cerdito 10\",\"Description\":\"Esta es la descripcion del perro 10\",\"ConsultationPlace\":\"Calle 20 avenida 30 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 10 pulgas\",\"RegistrationDate\":\"\\/Date(1515796127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519252127514)\\/\"},{\"IdConsultation\":11,\"IdCustomer\":111,\"CustomerName\":\"Nombre 11\",\"PeludoName\":\"Cerdito 11\",\"Description\":\"Esta es la descripcion del perro 11\",\"ConsultationPlace\":\"Calle 22 avenida 33 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 11 pulgas\",\"RegistrationDate\":\"\\/Date(1515623327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519424927514)\\/\"},{\"IdConsultation\":12,\"IdCustomer\":112,\"CustomerName\":\"Nombre 12\",\"PeludoName\":\"Perro 12\",\"Description\":\"Esta es la descripcion del perro 12\",\"ConsultationPlace\":\"Calle 24 avenida 36 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 12 pulgas\",\"RegistrationDate\":\"\\/Date(1515450527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519597727514)\\/\"},{\"IdConsultation\":13,\"IdCustomer\":113,\"CustomerName\":\"Nombre 13\",\"PeludoName\":\"Cerdito 13\",\"Description\":\"Esta es la descripcion del perro 13\",\"ConsultationPlace\":\"Calle 26 avenida 39 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 13 pulgas\",\"RegistrationDate\":\"\\/Date(1515277727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519770527514)\\/\"},{\"IdConsultation\":14,\"IdCustomer\":114,\"CustomerName\":\"Nombre 14\",\"PeludoName\":\"Cerdito 14\",\"Description\":\"Esta es la descripcion del perro 14\",\"ConsultationPlace\":\"Calle 28 avenida 42 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 14 pulgas\",\"RegistrationDate\":\"\\/Date(1515104927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1519943327514)\\/\"},{\"IdConsultation\":15,\"IdCustomer\":115,\"CustomerName\":\"Nombre 15\",\"PeludoName\":\"Gato 15\",\"Description\":\"Esta es la descripcion del perro 15\",\"ConsultationPlace\":\"Calle 30 avenida 45 \",\"GetNotification\":false,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 15 pulgas\",\"RegistrationDate\":\"\\/Date(1514932127514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520116127514)\\/\"},{\"IdConsultation\":16,\"IdCustomer\":116,\"CustomerName\":\"Nombre 16\",\"PeludoName\":\"Perro 16\",\"Description\":\"Esta es la descripcion del perro 16\",\"ConsultationPlace\":\"Calle 32 avenida 48 \",\"GetNotification\":true,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 16 pulgas\",\"RegistrationDate\":\"\\/Date(1514759327514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520288927514)\\/\"},{\"IdConsultation\":17,\"IdCustomer\":117,\"CustomerName\":\"Nombre 17\",\"PeludoName\":\"Cerdito 17\",\"Description\":\"Esta es la descripcion del perro 17\",\"ConsultationPlace\":\"Calle 34 avenida 51 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 17 pulgas\",\"RegistrationDate\":\"\\/Date(1514586527514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520461727514)\\/\"},{\"IdConsultation\":18,\"IdCustomer\":118,\"CustomerName\":\"Nombre 18\",\"PeludoName\":\"Gato 18\",\"Description\":\"Esta es la descripcion del perro 18\",\"ConsultationPlace\":\"Calle 36 avenida 54 \",\"GetNotification\":true,\"SendNotification\":true,\"AditionalInformation\":\"Este peludo tiene 18 pulgas\",\"RegistrationDate\":\"\\/Date(1514413727514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520634527514)\\/\"},{\"IdConsultation\":19,\"IdCustomer\":119,\"CustomerName\":\"Nombre 19\",\"PeludoName\":\"Cerdito 19\",\"Description\":\"Esta es la descripcion del perro 19\",\"ConsultationPlace\":\"Calle 38 avenida 57 \",\"GetNotification\":false,\"SendNotification\":false,\"AditionalInformation\":\"Este peludo tiene 19 pulgas\",\"RegistrationDate\":\"\\/Date(1514240927514)\\/\",\"ConsultationDateAndHour\":\"\\/Date(1520807327514)\\/\"}]}";

                        //JSONObject respuestaJSON = new JSONObject(json);
                         JSONObject respuestaJSON = new JSONObject(result.toString());
                        String resultJSON = respuestaJSON.getString("Status");

                        if(resultJSON.equals("1")){
                            ResultLogin = respuestaJSON.getString("ResultLogin");
                        }
                        devuelve = respuestaJSON.getString("Message");;
                    }
                    else
                        devuelve = "Revisa la conexion a internet e inténtalo nuevamente";
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            return devuelve;
        }
    }
}

